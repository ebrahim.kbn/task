import React from "react";
import "./Products.css";
import homes from "../data";
import Product from "./Product";
import Grid from "@material-ui/core/Grid";

const Products = () => {
  return (
    <Grid container spacing={2}>
      {homes.map((home) => (
        <Product key={home.id} home={home} />
      ))}
    </Grid>
  );
};

export default Products;
