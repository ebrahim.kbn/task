import { Grid } from "@material-ui/core";
import React from "react";
import "./Product.css";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import BookmarkBorderIcon from "@material-ui/icons/BookmarkBorder";
import KingBedIcon from "@material-ui/icons/KingBed";
import WidgetsIcon from "@material-ui/icons/Widgets";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";
import MobileStepper from "@material-ui/core/MobileStepper";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 140,
    width: 100,
  },
  control: {
    padding: theme.spacing(2),
  },
  imageContainer: {
    position: "relative",
    borderRadius: theme.spacing(1),
  },
  imageSave: {
    position: "absolute",
    left: "1rem",
    bottom: "-1.5rem",
  },
  imageText: {
    position: "absolute",
    right: "0",
    bottom: ".15rem",
    color: "white",
    backgroundColor: "rgba(0,0,0,.15)",
    padding: "0 1rem 1rem 1rem",
  },
  card: {
    border: "1px solid lightgray",
    borderRadius: theme.spacing(1),
  },
  row: {
    padding: theme.spacing(2),
  },
  topRow: {
    display: "flex",
    alignItems: "center",
  },
  leftContent: {
    paddingRight: theme.spacing(1.5),
  },
  imageStepper: {
    position: "absolute",
    right: "0",
    bottom: ".15rem",
    left: 0,
  },
}));

const Product = ({ home }) => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const theme = useTheme();

  const maxSteps = home.images.length;

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    <Grid item xs={12} sm={6}>
      <Grid item className={classes.card}>
        <div className={classes.imageContainer}>
          {maxSteps >= 2 ? (
            <div className={classes.imageContainer}>
              <AutoPlaySwipeableViews
                axis={theme.direction === "rtl" ? "x-reverse" : "x"}
                index={activeStep}
                onChangeIndex={handleStepChange}
                enableMouseEvents
              >
                {home.images.map((step, index) => (
                  <div
                    key={index}
                    style={{ width: "100%", borderRadius: "8px 8px 0 0" }}
                  >
                    {Math.abs(activeStep - index) <= 2 ? (
                      <img
                        style={{
                          width: "100%",
                          borderRadius: "8px 8px 0 0",
                          display: "block",
                          overflow: "hidden",
                        }}
                        src={step}
                        alt=""
                      />
                    ) : null}
                  </div>
                ))}
              </AutoPlaySwipeableViews>
              {/* <div className={classes.actions}>
                <IconButton
                  size="small"
                  onClick={handleBack}
                  disabled={activeStep === 0}
                  className={classes.actionsButton}
                >
                  {theme.direction === "rtl" ? (
                    <KeyboardArrowRight />
                  ) : (
                    <KeyboardArrowLeft />
                  )}
                </IconButton>
                <IconButton
                  size="small"
                  onClick={handleNext}
                  disabled={activeStep === maxSteps - 1}
                  className={classes.actionsButton}
                >
                  {theme.direction === "rtl" ? (
                    <KeyboardArrowLeft />
                  ) : (
                    <KeyboardArrowRight />
                  )}
                </IconButton>
              </div> */}
            </div>
          ) : (
            <img
              style={{ width: "100%", borderRadius: "8px 8px 0 0" }}
              src={home.images[0]}
              alt=""
            />
          )}
          <div className={classes.imageSave}>
            <Fab
              color="inherit"
              style={{
                backgroundColor: "white",
                height: "3rem",
                width: "3rem",
              }}
              aria-label="add"
            >
              <BookmarkBorderIcon color="action" />
            </Fab>
          </div>

          <div className={classes.imageText}>
            <span style={{ fontWeight: "bolder" }}>{home.place}</span>{" "}
            <span
              style={{ color: "rgba(255,255,255,.6)", marginRight: ".5rem" }}
            >
              {home.time}
            </span>
          </div>

          {maxSteps >= 2 && (
            <div className={classes.imageStepper}>
              <MobileStepper
                steps={maxSteps}
                position="static"
                variant="dots"
                activeStep={activeStep}
                style={{
                  justifyContent: "center",
                  backgroundColor: "transparent",
                }}
              />
            </div>
          )}
        </div>

        <Grid container className={classes.row}>
          <Grid item xs={6} className={classes.topRow}>
            <div>
              <h4 style={{ fontWeight: "bolder" }}>{home.vadieh}</h4>
            </div>
            <div className={classes.leftContent}>
              <p style={{ fontWeight: "bold", color: "rgba(0,0,0,.7)" }}>
                ودیعه
              </p>
              <p style={{ color: "rgba(0,0,0,.5)" }}>میلیون تومان</p>
            </div>
          </Grid>
          <Grid item xs={6} className={classes.topRow}>
            <h4>{home.ejareh}</h4>
            <div className={classes.leftContent}>
              <p style={{ fontWeight: "bold", color: "rgba(0,0,0,.7)" }}>
                اجاره
              </p>
              <p style={{ color: "rgba(0,0,0,.5)" }}>میلیون تومان</p>
            </div>
          </Grid>
        </Grid>
        <Grid container className={classes.row}>
          <Grid item xs={6} style={{ display: "flex", alignItems: "center" }}>
            <WidgetsIcon
              color="action"
              style={{ width: "21px", height: "21px" }}
            />{" "}
            <h4 style={{ margin: "0 .5rem" }}>{home.size}</h4>
            <p style={{ fontWeight: "bold", color: "rgba(0,0,0,.7)" }}>متر</p>
          </Grid>
          <Grid item xs={6} style={{ display: "flex", alignItems: "center" }}>
            <KingBedIcon
              color="action"
              style={{ width: "21px", height: "21px" }}
            />{" "}
            <h4 style={{ margin: "0 .5rem" }}>{home.capacity}</h4>
            <p style={{ fontWeight: "bold", color: "rgba(0,0,0,.7)" }}>خواب</p>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Product;
