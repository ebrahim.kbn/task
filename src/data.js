import room1 from "./images/details-1.jpeg";
import room2 from "./images/details-2.jpeg";
import room3 from "./images/details-3.jpeg";
import room4 from "./images/details-4.jpeg";
import img1 from "./images/room-1.jpeg";
import img2 from "./images/room-2.jpeg";
import img3 from "./images/room-3.jpeg";
import img4 from "./images/room-4.jpeg";
import img5 from "./images/room-5.jpeg";
import img6 from "./images/room-6.jpeg";
import img7 from "./images/room-7.jpeg";
import img8 from "./images/room-8.jpeg";
import img9 from "./images/room-9.jpeg";
import img10 from "./images/room-10.jpeg";
import img11 from "./images/room-11.jpeg";
import img12 from "./images/room-12.jpeg";
const homes = [
  {
    id: "1",
    ejareh: 80,
    vadieh: 50,
    size: 72,
    time: "دیروز",
    place: "استاد معین",
    capacity: 2,

    images: [img8, img9],
  },
  {
    id: "2",
    ejareh: 2,
    vadieh: 65,
    size: 86,
    time: "دوساعت پیش",
    place: "ظفر",
    capacity: 1,

    images: [img2],
  },
  {
    id: "3",
    ejareh: 3,
    vadieh: 130,
    size: 75,
    time: "سه روز پیش",
    place: "استاد معین",
    capacity: 2,

    images: [img3, room4, img2, room3],
  },
  {
    id: "4",
    ejareh: 5,
    vadieh: 90,
    size: 65,
    time: "چهار ساعت پیش",
    place: "زعفرانیه",
    capacity: 3,

    images: [img4, room1, img6],
  },
  {
    id: "5",
    ejareh: 3,
    vadieh: 70,
    size: 65,
    time: "پنج ساعت پیش",
    place: "میرداماد",
    capacity: 3,

    images: [img4, room1, img6],
  },
];

export default homes;
