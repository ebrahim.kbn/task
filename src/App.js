import "./App.css";
import Products from "./components/Products";
import Container from "@material-ui/core/Container";

function App() {
  return (
    <Container maxWidth="md">
      <Products />
    </Container>
  );
}

export default App;
